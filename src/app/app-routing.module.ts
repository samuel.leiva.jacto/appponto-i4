import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login/login.module#LoginPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'lancamentos', loadChildren: './pages/lancamentos/lancamentos.module#LancamentosPageModule' },
  { path: 'lancamentos-novo', loadChildren: './pages/lancamentos-novo/lancamentos-novo.module#LancamentosNovoPageModule' },
  { path: 'empresas', loadChildren: './pages/empresas/empresas.module#EmpresasPageModule' },
  { path: 'empresas-nova', loadChildren: './pages/empresas-nova/empresas-nova.module#EmpresasNovaPageModule' },
  { path: 'qrcodes', loadChildren: './pages/qrcodes/qrcodes.module#QrcodesPageModule' },
  { path: 'qrcodes-novo', loadChildren: './pages/qrcodes-novo/qrcodes-novo.module#QrcodesNovoPageModule' },
  { path: 'usuarios', loadChildren: './pages/usuarios/usuarios.module#UsuariosPageModule' },
  { path: 'usuarios-novo', loadChildren: './pages/usuarios-novo/usuarios-novo.module#UsuariosNovoPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
