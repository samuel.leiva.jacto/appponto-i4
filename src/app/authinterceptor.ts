import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { switchMap, tap } from 'rxjs/operators';
import { UserService } from './services/user/user.service';
import { from } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(
        private auth: UserService,
        private router: Router,
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        return from(this.auth.getAuthorizationToken())
            .pipe(
                switchMap((authToken: any) => {
                    const authReq = req.clone({
                        headers: req.headers.set('Authorization', 'Bearer ' + authToken)
                    });

                    return next.handle(authReq).pipe(
                        tap((event: HttpEvent<any>) => {
                            if (event instanceof HttpResponse) { }
                        }, (err: any) => {
                            if (err instanceof HttpErrorResponse) {
                                if (err.status === 401) {
                                    this.router.navigate(['login']);
                                }
                            }
                        })
                    );
                })
            )
    }
}
