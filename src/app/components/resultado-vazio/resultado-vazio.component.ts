import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-resultado-vazio',
  templateUrl: './resultado-vazio.component.html',
  styleUrls: ['./resultado-vazio.component.scss'],
})
export class ResultadoVazioComponent implements OnInit {

  @Input() icon: string;
  @Input() text: string;

  constructor() {
    this.icon = "information-circle";
    this.text = "Nenhum resultado encontrado";
  }

  ngOnInit() { }

}
