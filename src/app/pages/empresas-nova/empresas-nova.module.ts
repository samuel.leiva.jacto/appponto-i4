import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EmpresasNovaPage } from './empresas-nova.page';

const routes: Routes = [
  {
    path: '',
    component: EmpresasNovaPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EmpresasNovaPage]
})
export class EmpresasNovaPageModule {}
