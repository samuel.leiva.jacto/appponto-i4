import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpresasNovaPage } from './empresas-nova.page';

describe('EmpresasNovaPage', () => {
  let component: EmpresasNovaPage;
  let fixture: ComponentFixture<EmpresasNovaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpresasNovaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpresasNovaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
