import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import { WarningService } from 'src/app/services/warning/warning.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string = null;
  password: string = null;
  btnEnabled: boolean = true;

  constructor(
    private router: Router,
    private userService: UserService,
    private warningService: WarningService
  ) { }

  ngOnInit() {
  }

  async login(form) {
    if (form.valid) {
      this.btnEnabled = false;
      await this.userService.login(this.username, this.password)
        .then(() => {
          form.reset();
          this.router.navigateByUrl('lancamentos');
        })
        .catch((err: any) => {
          this.warningService.handleError('Erro ao fazer login.', err);
        })
      this.btnEnabled = true;
    }
  }

}
