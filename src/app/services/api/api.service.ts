import { Injectable, Inject } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { APP_CONFIG_TOKEN, ApplicationConfig } from 'src/app/app.config';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl: string;

  headers = new HttpHeaders()
    .set('Content-Type', 'application/json; charset=utf-8')
    .set('Accept', 'application/json');

  constructor(
    public httpClient: HttpClient,
    @Inject(APP_CONFIG_TOKEN) private config: ApplicationConfig
  ) {
    this.apiUrl = config.apiUrl;
  }
}
