import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WarningService {
  private toast = null;
  private isLoading = false;

  constructor(
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController
  ) { }

  async showToast(message: string) {
    if (this.toast) {
      this.toast.dismiss();
      this.toast = null;
    }
    this.toast = await this.toastCtrl.create({
      message: message,
      closeButtonText: "OK",
      showCloseButton: true,
      duration: 2500,
      position: 'bottom'
    });
    this.toast.present();
  }

  async showLoading() {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      duration: 1500
    })
      .then((load: any) => {
        load.present()
          .then(() => {
            if (!this.isLoading) {
              load.dismiss();
            }
          });
      });
  }

  async dismissLoading() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss();
  }

  async handleError(message: string, error: HttpErrorResponse) {
    this.showToast(message + ' ' + error);
    console.log(message + ' ' + error);
  }
}
